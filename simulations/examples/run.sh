#!/bin/bash
if [[ -z $1 ]]
then
	echo -e "\e[31mError: target simulation ini file not set\e[0m"
	exit 1
fi
echo -e "\e[32mRunning simulation $1\e[0m"
../nesting -m -u Cmdenv -n ..:../../src:../../../inet/src:../../../inet/examples:../../../inet/tutorials:../../../inet/showcases --image-path=../../../inet/images -l ../../src/nesting -l ../../../inet/src/INET $1
