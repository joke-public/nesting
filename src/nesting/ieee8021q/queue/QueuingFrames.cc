//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "../queue/QueuingFrames.h"
#define COMPILETIME_LOGLEVEL omnetpp::LOGLEVEL_TRACE

namespace nesting {

Define_Module(QueuingFrames);

void QueuingFrames::initialize() {
    //Initialize the number of queues, which is equal to the number of "out"
    // vector gate. The default values is 8, if user has not specified it.
    numberOfQueues = gateSize("out");

    //Precondition: numberOfQueues must have a valid number, i.e <= number of
    // all possible pcp values.

    if (numberOfQueues > kNumberOfPCPValues || numberOfQueues < 1) {
        throw new cRuntimeError(
                "Invalid assignment of numberOfQueues. Number of queues should not "
                        "be bigger than the number of all possible pcp values!");
    }
    auto tciToQueueMappingXml = par("tciToQueueMapping").xmlValue();
    // if there are mappings, apply them, else default to usage of the pcp only
    auto mappings = tciToQueueMappingXml->getChildrenByTagName("mapping");
    if (tciToQueueMappingXml->getChildrenByTagName("default").empty() && mappings.size()) {
        throw new cRuntimeError(
                "No default value in the queue mapping provided! use the default tag");
    } else if(!tciToQueueMappingXml->getChildrenByTagName("default").empty()) {
        auto defaultQueueString = tciToQueueMappingXml->getFirstChildWithTag("default")->getNodeValue();
        defaultQueue = std::stoul(defaultQueueString);
    }
    if(mappings.size()) {
        for(long unsigned int i = 0; i < mappings.size(); i++) {
            auto tci_val = mappings[i]->getFirstChildWithTag("tci")->getNodeValue();
            unsigned int tci_int = std::stoul(tci_val, nullptr, 10);
            auto queue_idx = mappings[i]->getFirstChildWithTag("queue")->getNodeValue();
            unsigned int queue_idx_int = std::stoul(queue_idx, nullptr, 10);
            tciToQueueMapping[tci_int] = queue_idx_int;
        }
        useTci = true;
    }
}

void QueuingFrames::handleMessage(cMessage *msg) {
    if (dynamic_cast<inet::Request*>(msg)) {
        delete msg;
        return;
    }

    inet::Packet *packet = check_and_cast<inet::Packet *>(msg);

    // switch ingoing VLAN Tag to outgoing Tag
    auto vlanTagIn = packet->removeTag<VLANTagInd>();
    int pcpValue = vlanTagIn->getPcp();
    short vid = vlanTagIn->getVID();
    bool dei = vlanTagIn->getDe();

    auto vlanTagOut = packet->addTag<VLANTagReq>();
    vlanTagOut->setPcp(pcpValue);
    vlanTagOut->setDe(vlanTagIn->getDe());
    vlanTagOut->setVID(vlanTagIn->getVID());

    // switch ingoing MAC Tag to outgoing MAC Tag
    auto macTagIn = packet->removeTag<inet::MacAddressInd>();
    auto macTagOut = packet->addTag<inet::MacAddressReq>();
    macTagOut->setDestAddress(macTagIn->getDestAddress());
    macTagOut->setSrcAddress(macTagIn->getSrcAddress());

    // switch ingoing sap tag to outgoing sap tag
    auto sapTagIn = packet->removeTag<inet::Ieee802SapInd>();
    auto sapTagOut = packet->addTag<inet::Ieee802SapReq>();
    sapTagOut->setDsap(sapTagIn->getDsap());
    sapTagOut->setSsap(sapTagIn->getSsap());
    delete macTagIn;
    delete vlanTagIn;
    delete sapTagIn;

    // remove encapsulation
    packet->trim();

    // Check whether the PCP value is correct.
    if (pcpValue > kNumberOfPCPValues) {
        throw new cRuntimeError(
                "Invalid assignment of PCP value. The value of PCP should not be "
                        "bigger than the number of supported queues.");
    }

    int queueIndex;
    if(useTci) {
        //
        // see https://en.wikipedia.org/wiki/IEEE_802.1Q#Frame_format
        // VLAN TAG
        // | 16 bits | 3 bits | 1 bit | 12 bits |
        // | TPID    | TCI                      |
        // | TPID    | PCP    | DEI   | VID     |
        // tci size: 16 bits = 2 bytes < size of int (whole vlan tag is 32 bit wide)
        unsigned int tci = (0xFFF & vid) + (dei << 12) + ((pcpValue & 0x7) << 13);
        auto tmp = tciToQueueMapping.find(tci);
        if (tmp != tciToQueueMapping.end()) {
            queueIndex = tmp->second;
        } else {
            queueIndex = defaultQueue;
        }
    } else {
        // Get the corresponding queue from the 2-dimensional matrix
        // standardTrafficClassMapping.
        queueIndex = this->standardTrafficClassMapping[numberOfQueues - 1][pcpValue];
    }



    // Get the corresponding gate and transmit the frame to it.
    EV_TRACE << getFullPath() << ": Sending packet '" << packet
                    << "' with pcp value '" << pcpValue << "' to queue "
                    << queueIndex << endl;

    cGate* outputGate = gate("out", queueIndex);
    send(msg, outputGate);
}

} // namespace nesting
