//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "FaultyClock.h"

namespace nesting {

Define_Module(FaultyClock);

void FaultyClock::initialize() {
    ClockBase::initialize();
    jitter = simTime().parse(par("jitter"));
    offset = simTime().parse(par("offset"));
}

ClockBase::ScheduledTick FaultyClock::lastTick() {
    ScheduledTick result;
    simtime_t elapsedTime = simTime() - lastGlobalTickTimestamp;
    result.ticks = elapsedTime.raw() / clockRate.raw();
    result.timestamp = lastGlobalTickTimestamp + result.ticks * clockRate;
    return result;
}

simtime_t FaultyClock::scheduleTick(unsigned int idleTicks) {
    if (lastGlobalTickTimestamp == 0 && idleTicks == 0) {
        return lastGlobalTickTimestamp + clockRate * idleTicks;
    }
    auto tmp = jitter / clockRate;
    if (tmp > 0) {
        //auto globalTimestamp = simTime() / clockRate;
        //int min = std::min(globalTimestamp, tmp) + 1;
        int random_number = std::experimental::randint<int>(-1 * tmp, tmp);
        auto result =  lastGlobalTickTimestamp + clockRate * idleTicks + offset + random_number * clockRate;
        auto test = result < simTime();
        if (test) {
            return simTime() + clockRate;
        }
        return result;
    }
    return lastGlobalTickTimestamp + clockRate * idleTicks + offset;
}

} // namespace nesting
